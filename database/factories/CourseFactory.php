<?php

use Faker\Generator as Faker;

$factory->define(App\Course::class, function (Faker $faker) {
    return [
        'course_id'=> $faker->unique()->randomNumber(3),
        'course_name'=> $faker->word,
        'course_type'=> $faker->word 
    ];
});
