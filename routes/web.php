<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/dev/{g_id}/{g_name}',function($g_id,$g_name)
{
    return 'The user name is '.$g_name. ' with the id '.$g_id ;
});

Route::get('/gurdian_home', 'GurdianController@index_gurdian')->name('home_gurdian');
Route::get('/show_gurdian', 'GurdianController@show_gurdian')->name('show_gurdian');
Route::post('/add_gurdian', 'GurdianController@add_gurdian')->name('add_gurdian');
Route::get('/edit_gurdian/{g_id}','GurdianController@editGurdian')->name('edit_gurdian');
Route::post('/update_gurdian','GurdianController@updateGurdian')->name('update_gurdian');
Route::get('/insert_gurdian','GurdianController@gurdian_insert_view')->name('insert_gurdian');
Route::get('/delete_gurdian/{g_id}','GurdianController@deleteGurdian')->name('delete_gurdian');

Route::get('/ww', function () {
    return view('welcome');
});
Route::get('/teacher_home','TeacherController@homeTeacher')->name('home_teacher');
Route::get('/show_teacher','TeacherController@showTeacher')->name('show_teacher');
Route::get('/insert_teacher','TeacherController@teacher_insert_view')->name('insert_teacher');
Route::post('/create_teacher','TeacherController@createTeacher')->name('create_teacher');
Route::get('/','TeacherController@index')->name('all_home');
Route::post('/update_teacher','TeacherController@updateTeacher')->name('update_teacher');
Route::get('/edit_teacher/{id}','TeacherController@editTeacher')->name('edit_teacher');
Route::get('/delete_teacher/{id}','TeacherController@deleteTeacher')->name('delete_teacher');

Route::get('course','CourseController@indexeli')->name('indexeli');
Route::get('course/{course}/edit','CourseController@editeli')->name('editeli');
Route::get('course/create','CourseController@createeli')->name('createeli');
Route::put('course/{course}','CourseController@updateeli')->name('updateeli');
Route::delete('course/{course}','CourseController@destroyeli')->name('destroyeli');
Route::post('course','CourseController@storeeli')->name('storeeli');

Route::get('/show_student','StudentformController@show_student')->name('show_student');
Route::get('/insert_student','StudentformController@student_insert_view')->name('insert_student');
Route::post('/store_student','StudentformController@store_student')->name('store_student');
Route::get('/home_student','StudentformController@index_student')->name('home_student');
Route::post('/update_student','StudentformController@update_student')->name('update_student');
Route::get('/edit_student/{id}','StudentformController@edit_student')->name('edit_student');
Route::get('/delete_student/{id}','StudentformController@delete_student')->name('delete_student');


Route::get('/Insert_Info_Add', 'StaffController@staff_insert_view')->name('create1_staff');
Route::get('/Show_staff', 'StaffController@show')->name('show_staff');
Route::post('/staff', 'StaffController@create')->name('create_staff');
Route::get('/home_staff','StaffController@index')->name('home_staff');
Route::post('/update_staff','StaffController@updateStaff')->name('update_staff');
Route::get('/edit_staff/{id}','staffController@editStaff')->name('edit_staff');
Route::get('/delete_staff/{id}','staffController@destroy')->name('delete_staff');
