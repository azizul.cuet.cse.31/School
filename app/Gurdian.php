<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gurdian extends Model
{
    protected $table = 'gurdians';
    protected $fillable = ['g_name','g_profession'];
}
