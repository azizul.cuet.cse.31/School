<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');

    }

    
    public function staff_insert_view()
    {
        return view('staff');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
         \App\Staff::create([
            's_name'=>$req->get('name'),
            's_designation'=>$req->get('designation')]);
        return redirect('/Insert_Info_Add')->with('success', 'Information has been added');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        return view('showStaff',['allstaffs'=> \App\Staff::all()]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
        public function editStaff($id){
            $staffInfo = \App\Staff::where('s_id',$id)->first();
            
            return view('edit_staff',['staffInfo' =>$staffInfo]);
          }
      
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
        public function updateStaff(Request $request)
        {
        
          $staffName = $request->get('s_name');
          $s_designation = $request->get('s_designation');
          $id = $request->get('s_id');
          $update = \App\staff::where('s_id', $id)
              ->update(['s_name' => $staffName,'s_designation' => $s_designation]);
          if($update){
            return redirect('/Show_staff')->with('success', 'Information has been updated');
          }else{
            return redirect('/Show_staff')->with('error', 'Sorry Failed!!!');
          }
            //
        }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staffdelete=\App\staff::where('s_id',$id);
        $staffdelete->delete();
        return redirect('/Show_staff')->with('success','info deleted');
    }
}
