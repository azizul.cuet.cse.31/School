<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GurdianController extends Controller
{

    public function index_gurdian() {
        return view('welcome');
    }

    public function show_gurdian() {
        return view('gurdian',['allGurdian'=>  \App\Gurdian::all()]);
    }

    public function add_gurdian(Request $request) {


        \App\Gurdian::create([
            'g_name'=>$request->get('g_name'),
            'g_profession'=>$request->get('g_profession')
        ]);
        return redirect('/gurdian_home')->with('success', 'Information has been added');
    }
    public function deleteGurdian($id)
    {
        $gurdianDelete = \App\Gurdian::where('g_id',$id);
        $gurdianDelete->delete();
        return redirect('/show-gurdian')->with('success','Information has been  deleted');
    }

    public function gurdian_insert_view()
    {
        return view('gurdian');
    }
    public function editGurdian($g_id){
        $gurdianInfo = \App\Gurdian::where('g_id', $g_id)->first();
        return view('edit_gurdian',['gurdianInfo' =>$gurdianInfo]);
    }


    public function updateGurdian(Request $request)
    {
        $gurdianName = $request->get('g_name');
        $gurdianProfession= $request->get('g_profession');
        $g_id = $request->get('g_id');
        $update = \App\Gurdian::where('g_id', $g_id)->update(['g_name' => $gurdianName,'g_profession'=>$gurdianProfession]);
        if($update){
            return redirect('/show-gurdian')->with('success', 'Information has been updated');
        }
        else{
            return redirect('/show-gurdian')->with('error', 'Sorry Failed!!!');
        }    //

    }
}
