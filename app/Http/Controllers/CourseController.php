<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function indexeli()
    {
        $all = Course::all();
        //dd($all);
        return view('course.aindex',['data'=>$all]);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createeli()
    {
        return view('course.acreate');
    }

    public function storeeli(Request $request)
    {
        $this->validate($request, [
            'id'=>'required|min:3',
            'name'=>'required',
            'type'=>'required'

    
       ]);
        //return 'dhaka';
       Course::create([
           'course_id'=>$request->id,
           'course_name'=>$request->name,
           'course_type'=>$request->type
       ]);

       return redirect(route('indexeli'))->with('success','course info has been added');
        
    }
    public function editeli($course)
    {
        //dd($course);
        $course = Course::find($course);
        return view('course.aedit',['id'=>$course]);
    }

    public function updateeli(Request $request, Course $course)
    {
       $course->course_id = $request->id;
       $course->course_name = $request->name;
       $course->course_type = $request->type;

       $course->save();
       return redirect(route('indexeli'))->with('success','course info has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroyeli(Course $course)
    {
        $course->delete();
        return redirect(route('indexeli'))->with('success','course info has been deleted');
    }
}
