<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('homepage.index');
    }
    public function homeTeacher()
    {
        return view('home_teacher');
    }
    public function teacher_insert_view()
    {
        return view('teacher');
    }
    public function showTeacher()
    {
        return view('show_teachers',['allteachers'=> \App\Teacher::all()]);
    }

    public function createTeacher(Request $req)
    {
        \App\Teacher::create(['tchr_name'=>$req->get('tchr_name'),'tchr_designation'=>$req->get('tchr_designation')]);
        return redirect('/teacher_show')->with('success', 'Information has been added');
    }
    public function editTeacher($id){
      $teacherInfo = \App\Teacher::where('tchr_id', $id)->first();
      return view('edit_teacher',['teacherInfo' =>$teacherInfo]);
    }

    public function updateTeacher(Request $request)
    {
      $teacherName = $request->get('tchr_name');
      $teacherDes= $request->get('tchr_designation');
      $id = $request->get('tchr_id');
      $update = \App\Teacher::where('tchr_id', $id)->update(['tchr_name' => $teacherName,'tchr_designation'=>$teacherDes]);
      if($update){
        return redirect('/teacher_show')->with('success', 'Information has been updated');
      }
      else{
        return redirect('/insert_teacher')->with('error', 'Sorry Failed!!!');
      }    //
    }

    public function deleteTeacher($id)
    {
      $teacherDelete = \App\Teacher::where('tchr_id',$id);
      $teacherDelete->delete();
       return redirect('/teacher_show')->with('success','Information has been  deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
