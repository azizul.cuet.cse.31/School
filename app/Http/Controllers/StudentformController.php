<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_student()
    {
        return view('home_student');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function student_insert_view()
    {
        return view('studentform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_student(Request $req)
    {
        \App\Student::create(['std_name'=>$req->get('name'), 'roll'=>$req->get('roll'), 'class'=>$req->get('class')]);
        return redirect('/insert_student')->with('success', 'Information has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_student()
    {
        return view('studentshow',['allstudents'=> \App\Student::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_student($id)
    {
        $studentInfo = \App\Student::where('std_id', $id)->first();
      return view('studentedit',['studentInfo' =>$studentInfo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_student(Request $request)
    {
        $studentName = $request->get('name');
        $studentRoll = $request->get('roll');
        $studentClass = $request->get('class');
      $id = $request->get('id');
      $update = \App\Student::where('std_id', $id)->update(['std_name' => $studentName, 'class'=>$studentClass, 'roll'=>$studentRoll]);
      if($update){
        return redirect('/show_student')->with('success', 'Information has been updated');
      }else{
        return redirect('/show_student')->with('error', 'Sorry Failed!!!');
      }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
  public function delete_student($id)
    {
      $StudentDelete = \App\Student::where('std_id',$id);
      $StudentDelete->delete();
       return redirect('/show_student')->with('success','Information has been  deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
