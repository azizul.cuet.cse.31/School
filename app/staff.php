<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class staff extends Model
{
    protected $table ='staff';
    protected $fillable = [
        's_name', 's_designation',
    ];
}
