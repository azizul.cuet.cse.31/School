
<!DOCTYPE html>
<html lang="en">
<head>

    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #b1b7ba; text-align: center;">

<nav class="navbar navbar-inverse">
    <div style="font-size: 20px;"class="container-fluid">
        <div class="navbar-header">
            <a style="font-size: 50px;"  class="navbar-brand" href="homepage.index">School</a>
        </div>

        <ul class="nav navbar-nav">
            <li><a href="{{route("all_home")}}"> School </a></li>
            <li><a  href="{{route("show_gurdian")}}">Show Gurdian</a></li>
            <li><a  href="{{route("home_gurdian")}}">Gurdian Input Form</a></li>
        </ul>
    </div>
</nav>
<div class="container">


    <table style="border-collapse: separate; border-spacing: 30px;" >
        <tr>
            <th style="font-size: 25px;text-align: center;">Name</th>
            <th style="font-size: 25px; text-align: center">Profession</th>
            <th style="font-size: 25px ;text-align: center">Edit</th>
            <th style="font-size: 25px; text-align: center">Delete</th>

        </tr>
        @foreach($allGurdian as $each)

            <tr>
                <td style="font-size: 22px; text-align: center">{{$each->g_name}}</td>
                <td style="font-size: 22px; text-align: center">{{$each->g_profession}}</td>
                <td><a class="btn btn-info" href="{{route('edit_gurdian',$each->g_id)}}">Edit</a></td>
                <td><a class="btn btn-danger" href="{{route('delete_gurdian',$each->g_id)}}">Delete</a></td>

            </tr>



        @endforeach
    </table>


</body>
</html>
