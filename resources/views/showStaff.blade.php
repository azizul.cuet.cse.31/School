
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Laravel 5.6 CRUD Tutorial With Example  </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
  </head>
  <body>
    <div class="container">

<nav class="navbar navbar-inverse">
  <div class="container-fluid">

<ul class="nav navbar-nav">
<li><a href="{{route('all_home')}}"> School </a> </a></li>
      <li class="active"><a href="{{route("home_staff")}}">Home</a></li>
      
      <li><a href="{{route("create1_staff")}}">Input Another </a></li>
      
    
  </div>
</nav>

<div class="jumbotron">
<h2>Staff List Show</h2><br/>
</div>

      
      


        <table class="table">
            <tr>
              <th>Name</th>

              <th>Edit</th>

              <th>Delete</th>
            </tr>

        @foreach($allstaffs as $each)
          <tr>
            <td>{{$each->s_name}}</td>


            <td><a class="btn btn-info" href="{{route('edit_staff',$each->s_id)}}">Edit</a></td>


              <td><a class="btn btn-danger" href="{{route('delete_staff',$each->s_id)}}">Delete</a></td>
          </tr>

        @endforeach

      </table>
  </body>
</html>
