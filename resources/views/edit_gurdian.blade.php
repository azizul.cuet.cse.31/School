

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #b1b7ba; text-align: center;">

<nav class="navbar navbar-inverse">
    <div style="font-size: 20px;"class="container-fluid">
            <ul class="nav navbar-nav">
            <li><a href="{{route('all_home')}}"> School </a></li>
            <li><a  href="{{route("show_gurdian")}}">Show Gurdian</a></li>
            <li><a  href="{{route("home_gurdian")}}">Gurdian Input Form</a></li>
        </ul>
    </div>
</nav>

    <h2>Gurdian Information System</h2><br/>
    <form method="post" action="{{route("update_gurdian")}}">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="Name">Name:</label>
                <input type="hidden" name="g_id" value="{{$gurdianInfo->g_id}}">
                <input type="text" class="form-control" value="{{$gurdianInfo->g_name}}"  name="g_name">
                <label for="Profession">Profession:</label>
                <input type="text" class="form-control" value="{{$gurdianInfo->g_profession}}"  name="g_profession">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4" style="margin-top:10px">
                <button type="submit" class="btn btn-success"  >Submit</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $('#datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
</script>
</body>
</html>
