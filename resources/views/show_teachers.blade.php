
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Show Teacher Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
  </head>
  <body>
    <div class="container">
    <li><a href="{{route('all_home')}}"> School </a> </a></li>
      <a class="btn btn-link active" href="{{route("home_teacher")}}">Teacher Home</a>
      <a class="btn btn-link active" href="{{route("insert_teacher")}}">Add Another</a>
      <a class="btn btn-link active" href="{{route("home_teacher")}}">Teacher Home</a>
      <a class="btn btn-link active" href="{{route("insert_teacher")}}">Add Another</a>
      <h2>Teachers List Show</h2><br/>

        <table class="table">
            <tr>
              <th>Name</th>

              <th>Edit</th>

              <th>Delete</th>
            </tr>

        @foreach($allteachers as $each)
          <tr>
            <td>{{$each->tchr_name}}</td>
            <td>{{$each->tchr_designation}}</td>
            <td><a class="btn btn-info" href="{{route('edit_teacher',$each->tchr_id)}}">Edit</a></td>
            <td><a class="btn btn-danger" href="{{route('delete_teacher',$each->tchr_id)}}">Delete</a></td>
          </tr>

        @endforeach

      </table>
      </div>
  </body>
</html>
