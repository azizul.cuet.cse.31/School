
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Update Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
  </head>
  <body>
    <div class="container">
    <a class="btn" href="{{route('all_home')}}"> School </a>

      <a class="btn" href="{{route("home_teacher")}}">Home</a>
      <a class="btn" href="{{route("show_teacher")}}">Show Teachers </a>
      <a class="btn" href="{{route("home_teacher")}}">Home</a>
      <a class="btn" href="{{route("show_teacher")}}">Show Teachers </a>

      <h2>Teacher Data Input System</h2><br/>
      <form method="post" action="{{route("update_teacher")}}">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name">Name:</label>
            <input type="hidden" name="tchr_id" value="{{$teacherInfo->tchr_id}}">
            <input type="text" class="form-control" value="{{$teacherInfo->tchr_name}}"  name="tchr_name">
            <label for="Designation">Designation:</label>
            <input type="text" class="form-control" value="{{$teacherInfo->tchr_designation}}"  name="tchr_designation">
          </div>
        </div>

        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:10px">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
    </div>
    <script type="text/javascript">
        $('#datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
         });
    </script>
  </body>
</html>
