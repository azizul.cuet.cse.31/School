@push('styles')
 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootst
rap-datepicker.css" rel="stylesheet">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootst
rap-datepicker.js"></script>
 <link rel="stylesheet" href="{{asset('css/app.css')}}">
 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel=
 "stylesheet">

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 <style>
  .jumbotron {
   background-color: #f4511e; /* Orange */
   color: #ffffff;
   margin-top: 50px;
   margin-bottom: 50px;
  }
 </style>
@endpush
@extends('layout.main')
@section('container')

    <div class="jumbotron text-center">


        <h1>Laravel School</h1>
        <p>Welcome to our school</p>
        <div class="row">
            <div class="col-lg-12">
                <a class="btn btn-green" href="{{route('show_student')}}"> Show Student</a>
                <a class="btn btn-info" href="{{route('insert_student')}}"> Add Student</a>

            </div>

        </div>

    </div>



@endsection
@push('scripts')
 <script type="text/javascript">
     $('#datepicker').datepicker({
         autoclose: true,
         format: 'dd-mm-yyyy'
     });
 </script>
@endpush
