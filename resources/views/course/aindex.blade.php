@extends('course.amaster')

@section('body')
@if (session()->has('success'))
    <h6 class="alert alert-success mt-5">{{ session('success') }}</h6>
@endif
<table class="table table-bordered mt-5">
    <thead class="">
      <tr>
        <th>Course ID</th>
        <th>Course Name</th>
        <th>Course Type</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $x)
    <tr>
        <td>{{$x->course_id}}</td>
        <td>{{$x->course_name}}</td>
        <td>{{$x->course_type}}</td>
        <td class="pl-5">
        <a href ="{{route('editeli',$x->id)}}" class="btn btn-info ml-2">Edit</a></td>
        <td class="pl-5"><form onsubmit = "return confirm('are you sure you want to delete this info?')" action = "{{route('destroyeli', $x->id)}}" method ="post" class = "d-inline-block">
           @csrf
           {{method_field('delete')}}
           <button type = "submit" class="btn btn-danger">Delete</button>
           </form></td>
      </tr>
    
    @endforeach
    </tbody>
  </table>


@endsection