<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Courses</title>
</head>
<body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="{{route('all_home')}}">School</a>
           </div>
           <ul class="nav navbar-nav">
          
          <li class= "active"><a href = "{{route('indexeli')}}">Courses</a></li>
          <li><a href = "{{route('createeli')}}">Add new course</a></li>
          
        </ul>
      </div>
    </nav>
    



    <div class="container">
    <h1 class="mt-5">Course Information</h1>
  
    <div>
      @yield('body')
      
    </div>
    
    
</body>
</html>