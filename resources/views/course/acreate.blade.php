@extends('course.amaster')


@section('body')


<form class="border border-light p-5" action="{{route('storeeli')}}" method = "post">
@csrf

<h2 class=" mt-4 mb-4">Add new Course</h2>
@if($errors->all())
      <div class="alert alert-danger"> 
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
      </div>
@endif


<label for="materialLoginFormEmail">Course ID</label>
<input type="text" id="defaultLoginFormEmail" class="form-control mb-4" name="id" >
<label for="materialLoginFormEmail">Course Name</label>
<input type="text" id="defaultLoginFormPassword" class="form-control mb-4" name="name">
<label for="materialLoginFormEmail">Course Type</label>
<input type="text" id="defaultLoginFormPassword" class="form-control mb-4" name="type">

<button class="btn btn-info btn-block my-4" type="submit">Add</button>


</form>

@endsection