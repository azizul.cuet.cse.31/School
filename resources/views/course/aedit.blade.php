@extends('course.amaster')


@section('body')


<form class="border border-light p-5" action="{{route('updateeli',$id->id)}}" method = "post">
@csrf
@method('put')
<p class="h4 mb-4">Update Course</p>
@if($errors->all())
      <div class="alert alert-danger"> 
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
      </div>
@endif


<label for="materialLoginFormEmail">Course ID</label>
<input type="text" id="defaultLoginFormEmail" class="form-control mb-4" name="id" value = "{{$id->course_id}}" >
<label for="materialLoginFormEmail">Course Name</label>
<input type="text" id="defaultLoginFormPassword" class="form-control mb-4" name="name" value="{{$id->course_name}}">
<label for="materialLoginFormEmail">Course Type</label>
<input type="text" id="defaultLoginFormPassword" class="form-control mb-4" name="type" value="{{$id->course_type}}">

<button class="btn btn-info btn-block my-4" type="submit">Update</button>


</form>

@endsection