<body>
{{--navigation part--}}
    @include('layout.nav')
    @yield('container')
    @include('layout.footer')
</body>
